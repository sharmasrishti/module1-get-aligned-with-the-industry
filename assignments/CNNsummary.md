# CNN Summary


### Convolutional Neural Network 

In deep learning , **convolutional neural network** is a class of **deep neural networks**, most commonly apllied or used for image and text recognition and also for classification. They are highly used in areas like identification of objects, faces, and traffic signs apart from generating vision in self driving cars and etc.


### How does a CNN work ?

1.  In CNN, The whole process start by creating a filter which has different features like colour, size, shape and in case of bird and fish the feature can also be eyes, nose, foot and various other body parts.
2.  The whole process of applying the filter and convolution is done until we get a fine output which is good enough to classify the image.
3.  Then comes the activation function which helps to decide if the neuron would fire or not ie the neuron is active or not. There are many types of activation function the most commonly used is **Rectified Linear Unit (ReLU)**.
4.  After this feature maps which are the output containing the features above mentioned. this output we get from the convolution . So the feature maps are flattened so that they come in single vector which then becomes the first layer of deep neural network which is also known as the fully connected layer.
5.  At last we get an output layer which classifies the output and gives the probability of each class, for which normally **softmax** is used.


### Important Terms

* **Kernel size** (K): Kernel size refers to the width x height of the filter.
* **Stride** (S): Stride is the number of pixels shifts over the input matrix.
* **Zero padding** (pad): It refers to the process of symmetrically adding zeroes to the input matrix.
* **Number of filters** (F): It refers to the no. of neurons or to be more precise it is the neuron,s input weights form.
* **Flattening** : It is converting the data into a 1 dimensional array for inputting it to the next layer.


### Structure of a Convolutional Neural Network

![](https://www.mdpi.com/sensors/sensors-19-04933/article_deploy/html/images/sensors-19-04933-g001.png)



