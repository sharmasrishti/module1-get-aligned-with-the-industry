# DNN Summary


### Deep Neural Network
 
DNN which is known as **Deep Neural Network**. It is an artificial neural network with multiple layers between input and output layers.
There are three layers in a DNN :

* Input Layer
* Hidden Layer
* Output Layer

Every neuron has a specific activation number. Weights are assigned to each neuron of first layer. All the activations of the first layer are taken and then compute to the weighted sum of all.


### Structure of DNN

![](https://42xtjqm0qj0382ac91ye9exr-wpengine.netdna-ssl.com/wp-content/uploads/2019/07/attacking-deep-neural-networks.png)


### Activation Function 

It is a function of a node that defines the output of that node given an input or set of inputs.

![](https://missinglink.ai/wp-content/uploads/2018/11/activationfunction-1.png)
 
### Sigmoid Function

* Sigmoid function also known as the logistic function.The input to the function is transformed into a value between 0.0 and 1.0. Before applyling     the  activation function we add bias (constant).
* Bias neuron is a special neural added to each layer in the neural network.
* But since , the commonly used activation function is ReLU and it is easy to use so we apply the respective function instead of sigmoid function. The range of the ReLU function between 0 and a ie the activation number which is given , not necessary that it should be denoted by a it can be denoted by any variable.

![](https://miro.medium.com/max/1452/1*XxxiA0jJvPrHEJHD4z893g.png)

### Gradient Descent

Gradient descent is used to minimize the function by iteratively moving in the direction of steepest descent ie it tells the direction in which you should step to increase the function much faster.

![](https://miro.medium.com/max/1024/1*G1v2WBigWmNzoMuKOYQV_g.png)


### Backpropagation

The procedure of evaluating the expression of the derivative of the cost function as product of derivatives between each layer from left to right ie backwards.
It is basically an algorithm used for supervised learning of neural network using the gradient descent.

##### Steps:

1. Initiliaze weights and biases in the network.
2. Propogate the inputs forward (by applyin the activation function).
3. Backpropogate the error (by updating weights and biases).
4. Terminating condition(when the error is very small).

![](https://www.guru99.com/images/1/030819_0937_BackPropaga1.png)
