# Industry Product/Project 


### Product Name (I)
 
 Click Fraud Detection

### Product Link

[Link](https://github.com/LeadingIndiaAI/Click-Fraud-Detection)

### Product Short Description

Click Fraud - It is practice of repeatedly clicking on an advertisement hosted on a website. It is a type of fraud that occurs on the Internet in pay-per-click (PPC) online advertising.

The product detects the fraud using machine learning and deep learning methods to classify each click accuracy upto 98%.

### Product is combination of features
  
   * ID Recognition
   * Person Detection

### Product is provided by which company ?
   LeadingIndia.AI


### Product Name (II)

   Road Damage Detection

### Product Link

[Link](https://github.com/LeadingIndiaAI/Road-damage-detection)

### Product Short Description

The product helps in automatic detection of the damaged roads which is an important task in transport maintenance for driving safety assurance.
As a result the roads are kept in good condition which is vital for safe driving and also monitors the degradation of road conditions.

### Product is combination of features
 
   * Object Detection
   * Segmentation

### Product is provided by which company ?
   LeadingIndia.AI